import { Body, Controller, Get, Post, Param, Patch, Delete} from '@nestjs/common';
import { get } from 'http';
import { TodosService } from './todos.service';
import { CreaeTodoDto } from './dto/create-todo.dto';

@Controller('todos')
export class TodosController {
    constructor(private readonly todosService: TodosService) { }
     
    @Get(':id')
    findOne(@Param ('id') id: string) {
        return this.todosService.findOne(id);
     

    }
    @Get()
    findAll(): any[] {
        return this.todosService.findAll();
    }
    @Post()
    createTodo(@Body() newTodo: CreaeTodoDto) {
               this.todosService.create(newTodo);
    
    }
    @Patch(':id')
    updateTodo(@Param('id') id: string, @Body() todo: CreaeTodoDto) {
    return this.todosService.update(id, todo);
    }
    @Delete(':id')
    deleteTodo(@Param('id') id: string) {
        return this.todosService.delete(id);

    }
}