export class CreaeTodoDto {
    readonly id: number;
    readonly title: string;
    readonly done: boolean;
    readonly description?: string;
}